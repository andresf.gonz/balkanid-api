import { Field, ID, InputType } from 'type-graphql';

@InputType('TableColumnUpdateInputType')
export class TableColumnUpdateInputType {
  @Field(() => ID)
  id: number;

  @Field(() => Boolean)
  enabled: boolean;
}
