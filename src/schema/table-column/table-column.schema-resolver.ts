import { Arg, Mutation, Query, Resolver } from 'type-graphql';
import { TableColumnModel } from './table-column.model';
import { TableColumnUpdateInputType } from './table-column.update.input-type';

@Resolver(TableColumnModel)
export class TableColumnSchemaResolver {
  @Query(() => [TableColumnModel])
  allTableColumns(): Promise<TableColumnModel[]> {
    return TableColumnModel.find();
  }

  @Mutation(() => [TableColumnModel])
  updateTableColumns(
    @Arg('columnsValue', () => [TableColumnUpdateInputType])
    columnsValue: TableColumnUpdateInputType[],
  ): Promise<TableColumnModel[]> {
    const tableColumnRepo = TableColumnModel.getRepository();
    return Promise.all(
      columnsValue.map(async ({ id, enabled }) =>
        tableColumnRepo.merge(await tableColumnRepo.findOne(id), { enabled }).save(),
      ),
    );
  }
}
