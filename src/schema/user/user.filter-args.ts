import { ArgsType, Field } from 'type-graphql';

@ArgsType()
export class UserFilterArgs {
  @Field(() => String, { nullable: true, defaultValue: null })
  firstname: string;

  @Field(() => String, { nullable: true, defaultValue: null })
  lastname: string;

  @Field(() => Number, { nullable: true, defaultValue: null })
  age: number;

  @Field(() => String, { nullable: true, defaultValue: null })
  email: string;

  @Field(() => Boolean, { nullable: true, defaultValue: null })
  married: boolean;

  @Field(() => Boolean, { nullable: true, defaultValue: null })
  active: boolean;

  @Field(() => Boolean, { nullable: true, defaultValue: null })
  verified: boolean;
}
