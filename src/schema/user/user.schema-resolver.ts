import { Args, Query, Resolver } from 'type-graphql';
import { FindConditions, Like } from 'typeorm';

import { UserModel } from './user.model';
import { UserFilterArgs } from './user.filter-args';

@Resolver(UserModel)
export class UserSchemaResolver {
  @Query(() => [UserModel])
  allUsers(@Args() filterArgs: UserFilterArgs): Promise<UserModel[]> {
    const { age, email, firstname, lastname, married, verified, active } = filterArgs;

    const whereFilter: FindConditions<UserModel> = {};
    if (age !== null) whereFilter.age = age;
    if (email !== null) whereFilter.email = Like(`%${email}%`);
    if (firstname !== null) whereFilter.firstname = Like(`%${firstname}%`);
    if (lastname !== null) whereFilter.lastname = Like(`%${lastname}%`);
    if (married !== null) whereFilter.married = married;
    if (verified !== null) whereFilter.verified = verified;
    if (active !== null) whereFilter.active = active;

    return UserModel.find({ where: whereFilter });
  }
}
