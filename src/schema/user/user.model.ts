import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Field, ID, ObjectType } from 'type-graphql';

@Entity({ name: 'users' })
@ObjectType('User')
export class UserModel extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  readonly id: number;

  @Field(() => String)
  @Column()
  firstname: string;

  @Field(() => String)
  @Column()
  lastname: string;

  @Field(() => Number)
  @Column()
  age: number;

  @Field(() => String)
  @Column()
  email: string;

  @Field(() => Boolean)
  @Column()
  married: boolean;

  @Field(() => Boolean)
  @Column()
  active: boolean;

  @Field(() => Boolean)
  @Column()
  verified: boolean;
}
