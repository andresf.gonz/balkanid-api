import dotEnv from 'dotenv';
dotEnv.config();

import 'reflect-metadata';
import { createConnection } from 'typeorm';
import { buildSchema } from 'type-graphql';
import { ApolloServer } from 'apollo-server';
import { UserSchemaResolver } from '@schema/user';
import { TableColumnSchemaResolver } from '@schema/table-column';

(async () => {
  await createConnection({
    name: 'default',
    type: 'mysql',
    host: process.env.MYSQL_HOST,
    port: parseFloat(process.env.MYSQL_PORT as string),
    username: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    entities: ['./src/schema/**/*.model.ts'],
    migrations: ['./src/migrations/**/*.migration.ts'],
    migrationsRun: true,
    cli: {
      migrationsDir: './src/migrations',
    },
    synchronize: true,
    logging: false,
  });

  const schema = await buildSchema({
    resolvers: [UserSchemaResolver, TableColumnSchemaResolver],
  });

  const server = new ApolloServer({ schema });

  await server.listen(process.env.GRAPHQL_PORT || 8080);
})().then(() => {
  console.log(`Server running on http://localhost:${process.env.GRAPHQL_PORT}`);
});
