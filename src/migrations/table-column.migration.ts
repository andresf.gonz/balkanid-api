import { MigrationInterface, QueryRunner } from 'typeorm';
import { TableColumnModel } from '@schema/table-column';

export class TableColumn1640636706529 implements MigrationInterface {
  async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.clearTable('table_column');
  }

  async up(queryRunner: QueryRunner): Promise<void> {
    const repo = queryRunner.connection.getRepository(TableColumnModel);
    await repo.insert([
      { name: 'firstname', enabled: true },
      { name: 'lastname', enabled: true },
      { name: 'age', enabled: true },
      { name: 'email', enabled: true },
      { name: 'married', enabled: true },
      { name: 'active', enabled: true },
      { name: 'verified', enabled: true },
    ]);
  }
}
