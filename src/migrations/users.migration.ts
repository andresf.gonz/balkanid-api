import { MigrationInterface, QueryRunner } from 'typeorm';
import { UserModel } from '@schema/user';

export class Users1640639873065 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const repo = queryRunner.connection.getRepository(UserModel);
    await repo.insert([
      {
        firstname: 'Andres Felipe',
        lastname: 'Gonzalez Forero',
        age: 33,
        email: 'andresfgonzalez.1988@gmail.com',
        active: true,
        married: true,
        verified: true,
      },
      {
        firstname: 'Katherine',
        lastname: 'Urrego',
        age: 35,
        email: 'katherinurrego@gmail.com',
        active: true,
        married: true,
        verified: false,
      },
      {
        firstname: 'Dora',
        lastname: 'Forero',
        age: 59,
        email: 'domafobe@hotmail.com',
        active: false,
        married: false,
        verified: true,
      },
      {
        firstname: 'Leidy Carolina',
        lastname: 'Gonzalez',
        age: 27,
        email: 'leidylight@hotmail.com',
        active: false,
        married: false,
        verified: false,
      },
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.clearTable('users');
  }
}
